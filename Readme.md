# BrainfuckOS

Bootable Brainfuck interpreter that runs in x86 unreal mode.
For the old Mac version, see the [macos](https://gitlab.com/psm14/asm-bf/tree/macos) branch.

![Demo](demo.gif)

## Requirements

- YASM
- QEMU or something to boot the image on

## Building

Run `make`

## Usage

QEMU:
- `qemu-system-i386 -hda bfos.bin`

Native:
- `dd` it to the first couple sectors of something and boot!

## Limitations

See `src/memmap.s` to adjust program size and memory limits.
