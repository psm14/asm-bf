
interp:
  ; out ebx - screen position
  mov ecx, bf_mem.start
.clear_mem:
  mov dword [ecx], 0
  add ecx, 4
  cmp ecx, bf_mem.end
  jl .clear_mem

.initialize:
  mov ecx, bf_mem.mid
  mov edx, command_buffer.start - 1

.interp_loop:
  add edx, 1
  cmp edx, command_buffer.end
  jge .done

  mov al, byte [edx]
  or al, al
  jz .done
  cmp al, '>'
  je .right
  cmp al, '<'
  je .left
  cmp al, '+'
  je .increment
  cmp al, '-'
  je .decrement
  cmp al, '.'
  je .put
  cmp al, ','
  je .get
  cmp al, '['
  je .loop_start
  cmp al, ']'
  je .loop_end

.right:
  add ecx, 1
  cmp ecx, bf_mem.end
  jge .done
  jmp .interp_loop

.left:
  sub ecx, 1
  cmp ecx, bf_mem.start
  jl  .done
  jmp .interp_loop

.increment:
  mov al, byte [ecx]
  add al, 1
  mov byte [ecx], al
  jmp .interp_loop

.decrement:
  mov al, byte [ecx]
  sub al, 1
  mov byte [ecx], al
  jmp .interp_loop

.put:
  mov al, byte [ecx]
  cmp al, 10
  je  .put_newline

  call put_char
  jmp .interp_loop

.put_newline:
  mov  esi, ecx
  mov  edi, edx
  call newline
  mov  ecx, esi
  mov  edx, edi
  jmp  .interp_loop

.get:
  call get_char
  mov byte [ecx], al
  jmp .interp_loop

.loop_start:
  mov al, byte [ecx]
  or  al, al
  jnz .interp_loop

  mov esi, 1
.loop_start_iter:
  add edx, 1
  cmp edx, command_buffer.end
  jge .done

  mov al, byte [edx]
  cmp al, '['
  je  .loop_start_nest
  cmp al, ']'
  je  .loop_start_unnest
  jmp .loop_start_iter
.loop_start_nest:
  add esi, 1
  jmp .loop_start_iter
.loop_start_unnest:
  sub esi, 1
  or  esi, esi
  jnz .loop_start_iter
  jmp .interp_loop

.loop_end:
  mov al, byte [ecx]
  or  al, al
  jz  .interp_loop

  mov esi, 1
.loop_end_iter:
  sub edx, 1
  cmp edx, command_buffer.start
  jl .done

  mov al, byte [edx]
  cmp al, ']'
  je  .loop_end_nest
  cmp al, '['
  je  .loop_end_unnest
  jmp .loop_end_iter
.loop_end_nest:
  add esi, 1
  jmp .loop_end_iter
.loop_end_unnest:
  sub esi, 1
  or  esi, esi
  jnz .loop_end_iter
  jmp .interp_loop

.done:
  ret
