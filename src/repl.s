repl:
  sub esp,  0x4
  mov dword [esp], 0

  mov ebx, vga_text.start
  mov esi, welcome
  call print
  call newline
  mov esi, prompt
  call print

.keyloop:
  call update_cursor

  call get_char
  cmp al, 0x08 ; Backspace?
  je  .backspace
  cmp al, 0x0d ; Enter?
  je  .enter
  mov ecx, dword [esp]
  cmp ecx, command_buffer.len
  jge .keyloop
  jmp .print_key

.enter:
  mov ecx, dword [esp]
  mov byte [command_buffer.start + ecx], 0x00
  call newline
  call interp
  call newline

  mov dword [esp], 0
  mov esi, prompt
  call print
  jmp .keyloop

.backspace:
  cmp ebx, vga_text.start
  je .keyloop

  mov eax, dword [esp]
  cmp eax, 0
  je .keyloop ; Already at beginning

  sub ebx, 2
  mov al, 0x00
  call put_char

  mov ecx, dword [esp]
  sub ecx, 1
  mov dword [esp], ecx

  sub ebx, 2
  jmp .keyloop

.print_key:
  mov ecx, dword [esp]
  mov byte [command_buffer.start + ecx], al
  add ecx, 1
  mov dword [esp], ecx

  call put_char
  jmp .keyloop

print:
  ; esi - char* string
  ; ebx - out position
  ; clobbers: eax
.loop:
  mov al, byte [esi]
  add esi, 1
  or   al, al
  jz   .done
  call put_char
  jmp  .loop
.done:
  ret

get_char:
  ; returns char in al
  mov ah, 0
  int 0x16
  ret

put_char:
  ; al - char character
  ; ebx - out position
  ; clobbers: ax
  mov ah, color
  mov word [ebx], ax
  add ebx, 2
  call scroll
  ret

update_cursor:
  ; Simply updates the cursor to point to wherever the current position is
  ; ebx - out position
  ; clobbers: eax, ecx, edx
  call get_row_col
  mov  ecx, ebx

  mov dh, al
  mov dl, ah
  mov bh, 0
  mov ah, 2
  int 0x10

  mov ebx, ecx
  ret

newline:
  ; ebx - out position
  ; clobbers: eax, ecx, edx
  call get_row_col
  and eax, 0xff
  add eax, 1
  mov cl,  vga_text.cols
  mul cl
  shl eax, 1
  lea ebx, [eax + vga_text.start]
  call scroll
  ret

get_row_col:
  ; ebx - out position
  ; return:
  ; al - row
  ; ah - col
  ; clobbers: ecx, edx
  mov ecx, ebx
  mov eax, ecx
  mov edx, 0
  sub eax, vga_text.start
  shr eax, 1
  mov bl, 80
  cwd
  div bl

  mov ebx, ecx
  ret

scroll:
  ; ebx - out position
  ; clobbers: eax
  cmp ebx, vga_text.end
  jl  .done

  mov ebx, vga_text.start + (vga_text.cols * 2) ; Start of second line
.copy_loop:
  mov ax,  word [ebx]
  sub ebx, vga_text.cols * 2
  mov word [ebx], ax
  add ebx, vga_text.cols * 2 + 2
  cmp ebx, vga_text.end
  jl  .copy_loop

  mov ah, color
  mov al, 0x00
  mov ebx, vga_text.last_line
.clear_loop
  mov word [ebx], ax
  add ebx, 2
  cmp ebx, vga_text.end
  jl  .clear_loop

  mov ebx, vga_text.last_line
.done:
  ret

; Constant
welcome:  db 'Welcome to BrainFuckOS!',0
prompt:   db '$ ',0
color:    equ 0x0f
vga_text: 
.start:     equ 0xb8000
.cols:      equ 80
.rows:      equ 25
.end:       equ .start + (.cols * .rows * 2)
.last_line: equ .end - (.cols * 2)
