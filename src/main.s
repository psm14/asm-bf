bits 16

org 0x7c00
boot:
  mov ax, 0x2401
  int 0x15 ; enable A20 bit

  mov ax, 0x3
  int 0x10 ; set vga text mode 3 (also clears the screen)

  cli
  push ds ; Save real mode data segment (but somehow not the limit)

  lgdt [gdt.null + 2] ; NULL entry in GDT is a pointer to itself in the expected
                      ; format
  mov eax, cr0
  or  eax, 0x1
  mov cr0, eax ; Enable protected mode 

  jmp protected ; Jump to next instruction to prevent some kind of crash?
                ; Note that this is *NOT* a far jump

protected:
  mov ax, gdt.data_seg ; Load data segment with maxed limit
  mov ds, ax

  mov eax, cr0
  and al, 0xFE
  mov cr0, eax ; Enable real mode again

  pop ds ; Restore old data segment (except the new limit remains somehow)
  sti

unreal: ; Read the rest of the program in
  mov ah, 0x2
  mov al, additional_sectors
  mov ch, 0                  ; Cylinder (assume first)
  mov dh, 0                  ; Head
  mov cl, 2                  ; Sector
  ; dl is set by the BIOS and shouldn't have been clobbered above
  mov bx, stage2             ; Destination
  int 0x13
  jmp stage2                 ; This should now be loaded

gdt:
.null: ; NULL segment, use to stash the length and pointer to the GDT for `lgdt`
  dw 0x0
  dw .end - gdt
  dd gdt
.data:
  dw 0xFFFF
  dw 0x0
  db 0x0
  db 10010010b
  db 11001111b
  db 0x0
.end:
.data_seg: equ .data - gdt

; Extra things that fit into the first sector
%include 'memmap.s'
%include 'repl.s'

times 510 - ($ - $$) db 0 ; pad to 512 (counting the next two) bytes
dw 0xaa55                 ; magic number to make this bootable

; ------------------------------------------------------------------------------


stage2:
  call repl ; This was actually packed into stage1 but whatever

%include 'interp.s'

times 1024 - ($ - $$) db 0
additional_sectors: equ 1
