
; Just picking some numbers
command_buffer:
.start: equ 0x100000 ; 1 MB
.len:   equ 0x100000
.end:   equ .start + .len

bf_mem:
.start: equ command_buffer.end
.len:   equ 0x500000
.mid:   equ .start + (.len / 2)
.end:   equ .start + .len
