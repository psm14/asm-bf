all: bfos.bin

clean:
	rm bfos.bin

bfos.bin: src/*.s
	yasm -f bin -o bfos.bin src/main.s

.phony: all clean
